from django.db import models

# Create informations of books in Book model


class Book(models.Model):
    title = models.CharField(max_length=60)
    author = models.CharField(max_length=140, blank=True)
    rating = models.CharField(max_length=60, blank=True)
    voters = models.CharField(max_length=60, blank=True)
    price = models.CharField(max_length=60, blank=True)
    currency = models.CharField(max_length=60, blank=True)
    description = models.TextField()
    publisher = models.CharField(max_length=60, blank=True)
    page_count = models.CharField(max_length=60, blank=True)
    generes = models.CharField(max_length=60, blank=True)
    ISBN = models.CharField(max_length=60)
    language = models.CharField(max_length=60, blank=True)
    published_date = models.CharField(max_length=60, blank=True)

    def __str__(self):
        return self.title
