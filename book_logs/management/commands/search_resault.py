from django.core.management import BaseCommand
import pickle
import csv


# Make a commnd for save  a resault of searches in search.csv file


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Load list.pkl (list of searches)

        with open('list.pkl', 'rb') as f:
            mynewlist = (pickle.load(f))

        # Create search_count.csv file

        with open('search.csv', 'w') as fileCount:
            writer = csv.writer(fileCount)
            writer.writerow(['search'])
            for search in mynewlist:
                writer.writerow([search])
    

        self.stdout.write(self.style.SUCCESS('csv file saved'))
