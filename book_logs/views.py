import csv
from django.shortcuts import render
from .models import Book
from .serializers import BookSerializer
from rest_framework import generics
from django.http import HttpResponse
from django.db.models import Q
import pickle


# BookList and BookDetail class used a restful framwork for CRUD
class BookList(generics.ListCreateAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


class BookDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Book.objects.all()
    serializer_class = BookSerializer


# View function index for homepage and showed titel of books
# Make a search with query
# Make a list of searches in list.pkl file

searches = []


def index(request):
    global searches

    query = request.GET.get('q', None)
    books = Book.objects.order_by('pk')
    if query is not None:
        books = Book.objects.filter(
            Q(title__icontains=query)
        )
    context = {'books': books}
    searches.append(query)
    with open('list.pkl', 'wb') as f:
        pickle.dump(searches, f)

    return render(request, 'book_logs/index.html', context)

# View function book for detail of each books


def book(request, book_id):
    book = Book.objects.get(id=book_id)
    author = book.author
    rating = book.rating
    voters = book.voters
    price = book.price
    currency = book.currency
    description = book.description
    publisher = book.publisher
    page_count = book.page_count
    generes = book.generes
    ISBN = book.ISBN
    language = book.language
    published_date = book.published_date

    context = {'book': book, 'author': author,
               'rating': rating, 'voters': voters, 'price': price,
               'currency': currency, 'description': description,
               'publisher': publisher, 'page_count': page_count, 'generes': generes,
               'ISBN': ISBN, 'language': language,
               'published_date': published_date}
    return render(request, 'book_logs/book.html', context)
