from django.conf.urls import url
from django.urls import path
from . import views
from rest_framework.urlpatterns import format_suffix_patterns


app_name = 'book_logs'
urlpatterns = [
    # Homepage
    url(r'^$', views.index, name='index'),
    # Detail page for a single book
    url(r'^/(?P<book_id>\d+)/$', views.book, name='book'),
    # Rest framework path
    path('booklist/', views.BookList.as_view()),
    path('booklist/<int:pk>/', views.BookDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
