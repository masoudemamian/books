from django.apps import AppConfig


class BookLogsConfig(AppConfig):
    name = 'book_logs'
