from django.contrib import admin

from book_logs.models import Book

admin.site.register(Book)